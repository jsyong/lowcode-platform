// 属性配置类型声明
import { VisualEditorProps } from '../visual-editor-props/Index.d';
// 左侧组件配置类型声明
import { createVisualEditorConfig } from './Index.utils';

// 左侧都有哪些组件-配置
export interface VisualEditorComponent {
    key: string, // text、input、button、select...
    label: string, // 文本、输入框、按钮、下拉框...
    preview: () => JSX.Element, // 默认渲染
    render: (data: { // 画布渲染，给当前block添加属性
        props: any, // 属性
        model: any, // 绑定值
        size: { width?: number, height?: number }, // 宽高
        custom: Record<string, any> // 自定义属性
    }) => JSX.Element,
    props?: Record<string, VisualEditorProps>, // 绑定属性（用于右侧）
    model?: Record<string, string>, // 用于右侧绑定值的标识（用于右侧）
    resize?: { width?: boolean, height?: boolean } // 是否可拖拽改变宽高
}

// 左侧组件配置类型声明
export type VisualEditorConfig = ReturnType<typeof createVisualEditorConfig>;

