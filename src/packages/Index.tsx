import { defineComponent, PropType, ref, reactive, computed } from 'vue';
import './Index.scss';
// 组件
import VisualEditorHeader from './layout/visual-editor-header/Index';
import VisualEditorMenu from './layout/visual-editor-menu/Index'
import VisualEditorContent from './layout/visual-editor-content/Index'
import VisualEditorProps from './layout/visual-editor-props/Index'
import $$dialog from './components/import-export-dialog/Index'
import { ElMessageBox } from 'element-plus';
// 类型
import { VisualEditorModelValue, VisualEditorState } from './Index.d';
import { VisualEditorConfig } from './layout/visual-editor-menu/Index.d';
import { VisualEditorBlockData } from './components/visual-editor-block/Index.d';
// 注册方法
import { useVisualCommand } from './plugins/command/VisualCommand';
// 中间件
import { useModel } from './plugins/useModel';
// 其他
import { createEvent } from './utils/event';
import { VisualEventBusProvider, VisualDragProvider } from './Index.utils';


const VisualEditor = defineComponent({
    components: {
        VisualEditorHeader,
        VisualEditorMenu,
        VisualEditorContent,
        VisualEditorProps
    },
    props: {
        modelValue: {
            type: Object as PropType<VisualEditorModelValue>,
            required: true
        },
        // 配置都有哪些组件
        config: {
            type: Object as PropType<VisualEditorConfig>,
            required: true
        },
        // 绑定的数据
        formData: {
            type: Object as PropType<Record<string, any>>,
            required: true
        },
        // 自定义属性
        customProps: {
            type: Object as PropType<Record<string, any>>
        }
    },
    emits: {
        'update:modelValue': (val?: VisualEditorModelValue) => {
            // 改变modelValue的值，调用此方法，会发射到父级
            return true;
        }
    },
    setup(props, ctx) {
        // 将导入数据变为响应式，，监听数据变动，数据发生变化，同步到父级
        const dataModel = useModel(() => props.modelValue, val => ctx.emit('update:modelValue', val));
        // console.log('注册的组件dataModel', dataModel);

        // 订阅事件，用于左侧组件拖拽时，联动给容器添加事件
        const eventBus = createEvent();
        VisualEventBusProvider.provide({ eventBus });
        // 订阅事件，用于容器组件拖拽时，记录拖拽信息，实现撤销和重做
        const dragStart = createEvent();
        const dragEnd = createEvent();
        VisualDragProvider.provide({dragStart, dragEnd});

        // 当前选中操作的block(只保存一个，如果shift选择多个，则是最后一个)
        let selectIndex = ref(-1);
        const state: VisualEditorState = reactive({
            selectBlock: computed(() => (dataModel.value.blocks || [])[selectIndex.value]),
            preview: true, // 默认预览状态  预览-可输入  编辑-可拖拽
            editFlag: true  // 当前是否开启了编辑器
        })

        // 计算选中与未选中的block
        const focusData = computed(() => {
            let focus: VisualEditorBlockData[] = [];
            let unFocus: VisualEditorBlockData[] = [];
            (dataModel.value.blocks || []).forEach(block => (block.focus ? focus : unFocus).push(block))
            return {
                focus,
                unFocus
            }
        })
        // 公共方法
        const publicMethods = {
            // 更新 dataModel
            updateDataModel: (modelValue: VisualEditorModelValue) => {
                dataModel.value = modelValue;
            },
            // 清除选中的block
            clearFocus: (block?: VisualEditorBlockData) => {
                let blocks = (dataModel.value.blocks || []);
                if (!blocks.length) return;
                if (!!blocks) {
                    blocks = blocks.filter(item => item !== block);
                }
                blocks.forEach(block => block.focus = false);
            },
            // 更新block，渲染最新数据
            updateBlocks: (blocks?: VisualEditorBlockData[]) => {
                dataModel.value = {
                    ...dataModel.value,
                    blocks
                }
                // console.log('dataModel.value :>> ', dataModel.value);
            },
            // 更新单个block
            updateBlock: (newBlock: VisualEditorBlockData, oldBlock: VisualEditorBlockData) => {
                let oldBlocks = dataModel.value.blocks || [];
                // 找到位置，替换成新的block
                let blocks = [...oldBlocks];
                const index = oldBlocks.indexOf(oldBlock);
                if (index > -1) {
                    blocks.splice(index, 1, newBlock);
                }
                publicMethods.updateBlocks(blocks);
            },
            // 查看数据
            showBlockData: (block: VisualEditorBlockData) => {
                $$dialog.textarea(JSON.stringify(block), '节点数据', { editReadonly: true });
            },
            // 导入节点
            importBlockData: async (block: VisualEditorBlockData) => {
                const text = await $$dialog.textarea('', '请输入节点JSON字符串');
                try {
                    try {
                        const data = JSON.parse(text || '');
                        publicMethods.updateBlock(data, block);
                    } catch (e) {
                        console.error(e);
                        ElMessageBox.alert('解析JSON错误');
                    }
                } catch (e) {
                    console.error(e);
                }
            },
            // 重置selectIndex
            resetSelectIndex: (index: number) => {
                selectIndex.value = index;
            },
            // 修改预览或编辑状态
            changePreviewState: (isPreview: boolean) => {
                state.preview = isPreview;
            }
        }

        // 顶部功能-撤销重做、置顶置底...
        const commander = useVisualCommand({
            dataModel,
            focusData,
            updateBlocks: publicMethods.updateBlocks,
            dragStart,
            dragEnd
        });

        return () => <>
            <div class="visual-editor">
                <visual-editor-header
                    dataModel={ dataModel }
                    state={ state }
                    commander={ commander }
                    onUpdateDataModel={ publicMethods.updateDataModel }
                    onClearFocus={ publicMethods.clearFocus } // 清除聚焦
                    onChangePreviewState={ publicMethods.changePreviewState } // 清除聚焦
                />

                <section class="visual-editor-container">
                    <visual-editor-menu
                        componentList={ props.config.componentList }
                    />
                    <visual-editor-content
                        container={ dataModel.value.container }
                        blocks={ dataModel.value.blocks }
                        config={ props.config }
                        formData={ props.formData }
                        customProps={ props.customProps }
                        state={ state }
                        slots={ ctx.slots }
                        focusData={ focusData }
                        commander={ commander }
                        onUpdateBlocks={ publicMethods.updateBlocks } // 更新blocks
                        onClearFocus={ publicMethods.clearFocus } // 清除聚焦
                        onResetSelectIndex={ publicMethods.resetSelectIndex } // 修改focus的block
                        onShowBlockData={ publicMethods.showBlockData }
                        onImportBlockData={ publicMethods.importBlockData }
                    />
                    <visual-editor-props
                        block={ state.selectBlock }
                        config={ props.config }
                        dataModel={ dataModel }
                        onUpdateBlock={ publicMethods.updateBlock }
                        onUpdateDataModel={ publicMethods.updateDataModel }
                    />
                </section>
            </div>
        </>
    }
})

export default VisualEditor;
