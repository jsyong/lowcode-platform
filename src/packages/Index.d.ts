// block类型声明
import { VisualEditorBlockData } from './components/visual-editor-block/Index.d';
import { VisualEditorComponent } from './components/visual-editor-menu/Index';

// container类型声明
export interface VisualEditorContainer {
    width: number;
    height: number;
}

// modelValue
export interface VisualEditorModelValue {
    container: VisualEditorContainer;
    blocks?: VisualEditorBlockData[];
}

// state
export interface VisualEditorState {
    selectBlock: VisualEditorComponent,
    preview: boolean,
    editFlag: boolean
}

// focusData
export interface VisualEditorFocusData {
    focus: VisualEditorBlockData[] = [];
    unFocus: VisualEditorBlockData[] = [];
}

// 拖拽事件类型定义
export interface VisualDragEvent {
    dragStart: {
        on: (cb: () => void) => void,
        off: (cb: () => void) => void,
        emit: () => void,
    },
    dragEnd: {
        on: (cb: () => void) => void,
        off: (cb: () => void) => void,
        emit: () => void,
    }
}

// 左侧菜单和中间容器通信
export interface VisualEventBus {
    eventBus: { // 左侧组件开始拖拽， 给容器绑定事件
        on: (cb: (dragComponent?: any) => void) => void,
        off: (cb: (dragComponent?: any) => void) => void,
        emit: (dragComponent?: any) => void
    }
}