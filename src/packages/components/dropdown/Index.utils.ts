import { provide, inject } from 'vue';
import { DropdownEvent } from './Index.d';

// 选择一条选项，则关闭菜单（父子组件通信）
export const DropdownProvider = (() => {
    const DROPDOWN_PROVIDER = '@@DROPDOWN_PROVIDER';
    return {
        provide: (handler: DropdownEvent) => provide(DROPDOWN_PROVIDER, handler),
        inject: () => inject(DROPDOWN_PROVIDER) as DropdownEvent
    }
})();