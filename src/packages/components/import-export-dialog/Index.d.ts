// dialog
export interface DialogServiceOption {
    title?: string; // 标题
    editType: DialogServiceEditType; // 弹窗内部组件类型
    editReadonly?: boolean; // 是否只读，导入展示确认取消-否，导出-是
    editValue?: string | null; // 数据
    onConfirm: (val?: string | null) => void;
}




